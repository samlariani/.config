set relativenumber
set ruler
set ts=4 sts=4 sw=4 expandtab
syntax on
filetype plugin on
set cursorline
set showcmd
set wildmenu
set lazyredraw
set noswapfile
set nobackup

" set leader
nnoremap <SPACE> <Nop>
let mapleader=" "

" folding 
set foldmethod=indent
set foldnestmax=10
set nofoldenable "don't fold by default
set foldlevel=0
autocmd BufWinEnter *.* silent! loadview

"vim-plug
call plug#begin('~/.local/share/nvim/site/autoload/plugged')
Plug 'JuliaEditorSupport/julia-vim' 
Plug 'bling/vim-airline'
Plug 'vim-scripts/c.vim'
Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }
Plug 'neoclide/coc.nvim', {'branch': 'release'}
call plug#end()
"split navigation
noremap <C-J> <C-W><C-J>
noremap <C-k> <C-W><C-k>
noremap <C-H> <C-W><C-H>
noremap <C-L> <C-W><C-L>
noremap <C-Q> <C-W><C-Q>
"splits
nnoremap <leader>h :sp<CR>
nnoremap <leader>v :vsp<CR>

let g:airline_powerline_fonts = 1

"colorscheme  cobalt2

let g:livepreview_previewer = 'zathura'
