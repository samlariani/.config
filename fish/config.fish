set -U fish_key_bindings fish_vi_key_bindings

set -x PATH ~/.cargo/bin $PATH
neofetch
set -U budspencer_nogreeting
set -U EDITOR nvim

if test (uname -s) = "Darwin"
  set -gx PATH /usr/local/opt/coreutils/libexec/gnubin $PATH
  set -gx PATH /usr/local/opt/gnu-sed/libexec/gnubin $PATH
end
